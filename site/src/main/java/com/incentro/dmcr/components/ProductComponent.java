package com.incentro.dmcr.components;

import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.site.HstServices;
import org.onehippo.forge.selection.hst.contentbean.ValueList;
import org.onehippo.forge.selection.hst.manager.ValueListManager;
import org.onehippo.forge.selection.hst.util.SelectionUtil;

/**
 * Created by roelvanheel on 07-08-15.
 */
public class ProductComponent extends org.onehippo.cms7.essentials.components.EssentialsContentComponent {

    @Override
    public void doBeforeRender(final HstRequest request, final HstResponse response) {
        super.doBeforeRender(request, response);

        ValueListManager valueListManager = HstServices.getComponentManager()
                .getComponent(ValueListManager.class.getName());
        HippoBean siteContentBaseBean = request.getRequestContext()
                .getSiteContentBaseBean();

        ValueList valueList = valueListManager.getValueList(
                siteContentBaseBean, "categories");
        if (valueList != null) {
            request.setAttribute("categories",
                    SelectionUtil.valueListAsMap(valueList));
        }
    }

}
