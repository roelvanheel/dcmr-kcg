package com.incentro.dmcr.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoDocument;

@HippoEssentialsGenerated(internalName = "kcg:geolocation")
@Node(jcrType = "kcg:geolocation")
public class Geolocation extends HippoDocument {
	@HippoEssentialsGenerated(internalName = "kcg:latitude")
	public String getLatitude() {
		return getProperty("kcg:latitude");
	}

	@HippoEssentialsGenerated(internalName = "kcg:longitude")
	public String getLongitude() {
		return getProperty("kcg:longitude");
	}
}
