package com.incentro.dmcr.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoDocument;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import java.util.List;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;

@HippoEssentialsGenerated(internalName = "kcg:Project")
@Node(jcrType = "kcg:Project")
public class Project extends BaseDocument {
	@HippoEssentialsGenerated(internalName = "kcg:title")
	public String getTitle() {
		return getProperty("kcg:title");
	}

	@HippoEssentialsGenerated(internalName = "kcg:introduction")
	public String getIntroduction() {
		return getProperty("kcg:introduction");
	}

	@HippoEssentialsGenerated(internalName = "kcg:description")
	public HippoHtml getDescription() {
		return getHippoHtml("kcg:description");
	}

	@HippoEssentialsGenerated(internalName = "kcg:images")
	public List<HippoGalleryImageSet> getImages() {
		return getLinkedBeans("kcg:images", HippoGalleryImageSet.class);
	}

	@HippoEssentialsGenerated(internalName = "kcg:categories")
	public String[] getCategories() {
		return getProperty("kcg:categories");
	}
}
