package com.incentro.dmcr.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoDocument;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import java.util.List;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import com.incentro.dmcr.beans.Kcg;

@HippoEssentialsGenerated(internalName = "kcg:productdocument")
@Node(jcrType = "kcg:productdocument")
public class Productdocument extends BaseDocument {
	public final static String DOCUMENT_TYPE = "kcg:productdocument";
	private final static String TITLE = "kcg:title";
	private final static String PRICE = "kcg:price";
	private final static String INTRODUCTION = "kcg:introduction";
	private final static String DESCRIPTION = "kcg:description";
	private final static String IMAGES = "kcg:images";

	/** 
	 * Get the title of the document.
	 * @return the title
	 */
	@HippoEssentialsGenerated(internalName = "kcg:title")
	public String getTitle() {
		return getProperty(TITLE);
	}

	@HippoEssentialsGenerated(internalName = "kcg:price")
	public Double getPrice() {
		return getProperty(PRICE);
	}

	@HippoEssentialsGenerated(internalName = "kcg:introduction")
	public String getIntroduction() {
		return getProperty(INTRODUCTION);
	}

	@HippoEssentialsGenerated(internalName = "kcg:description")
	public HippoHtml getDescription() {
		return getHippoHtml(DESCRIPTION);
	}

	@HippoEssentialsGenerated(internalName = "kcg:images")
	public List<Kcg> getImages() {
		return getLinkedBeans("kcg:images", Kcg.class);
	}

	@HippoEssentialsGenerated(internalName = "kcg:categories")
	public String[] getCategories() {
		return getProperty("kcg:categories");
	}
}
