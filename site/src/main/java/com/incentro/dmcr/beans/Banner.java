package com.incentro.dmcr.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import com.incentro.dmcr.beans.Kcg;

@HippoEssentialsGenerated(internalName = "kcg:bannerdocument")
@Node(jcrType = "kcg:bannerdocument")
public class Banner extends BaseDocument {
	@HippoEssentialsGenerated(internalName = "kcg:title")
	public String getTitle() {
		return getProperty("kcg:title");
	}

	@HippoEssentialsGenerated(internalName = "kcg:content")
	public HippoHtml getContent() {
		return getHippoHtml("kcg:content");
	}

	@HippoEssentialsGenerated(internalName = "kcg:link")
	public HippoBean getLink() {
		return getLinkedBean("kcg:link", HippoBean.class);
	}

	@HippoEssentialsGenerated(internalName = "kcg:image")
	public Kcg getImage() {
		return getLinkedBean("kcg:image", Kcg.class);
	}
}
