package com.incentro.dmcr.beans;

import java.util.Calendar;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoDocument;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import com.incentro.dmcr.beans.Kcg;
import java.util.List;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import com.incentro.dmcr.beans.Geolocation;

@HippoEssentialsGenerated(internalName = "kcg:newsdocument")
@Node(jcrType = "kcg:newsdocument")
public class NewsDocument extends HippoDocument {
	/** 
	 * The document type of the news document.
	 */
	public final static String DOCUMENT_TYPE = "kcg:newsdocument";
	private final static String TITLE = "kcg:title";
	private final static String DATE = "kcg:date";
	private final static String INTRODUCTION = "kcg:introduction";
	private final static String IMAGE = "kcg:image";
	private final static String CONTENT = "kcg:content";
	private final static String LOCATION = "kcg:location";
	private final static String AUTHOR = "kcg:author";
	private final static String SOURCE = "kcg:source";

	/** 
	 * Get the title of the document.
	 * @return the title
	 */
	@HippoEssentialsGenerated(internalName = "kcg:title")
	public String getTitle() {
		return getProperty(TITLE);
	}

	/** 
	 * Get the date of the document.
	 * @return the date
	 */
	@HippoEssentialsGenerated(internalName = "kcg:date")
	public Calendar getDate() {
		return getProperty(DATE);
	}

	/** 
	 * Get the introduction of the document.
	 * @return the introduction
	 */
	@HippoEssentialsGenerated(internalName = "kcg:introduction")
	public String getIntroduction() {
		return getProperty(INTRODUCTION);
	}

	/** 
	 * Get the main content of the document.
	 * @return the content
	 */
	@HippoEssentialsGenerated(internalName = "kcg:content")
	public HippoHtml getContent() {
		return getHippoHtml(CONTENT);
	}

	/** 
	 * Get the location of the document.
	 * @return the location
	 */
	@HippoEssentialsGenerated(internalName = "kcg:location")
	public String getLocation() {
		return getProperty(LOCATION);
	}

	/** 
	 * Get the author of the document.
	 * @return the author
	 */
	@HippoEssentialsGenerated(internalName = "kcg:author")
	public String getAuthor() {
		return getProperty(AUTHOR);
	}

	/** 
	 * Get the source of the document.
	 * @return the source
	 */
	@HippoEssentialsGenerated(internalName = "kcg:source")
	public String getSource() {
		return getProperty(SOURCE);
	}

	@HippoEssentialsGenerated(internalName = "kcg:image")
	public Kcg getImage() {
		return getLinkedBean("kcg:image", Kcg.class);
	}

	@HippoEssentialsGenerated(internalName = "kcg:relatednews")
	public List<HippoBean> getRelatednews() {
		return getLinkedBeans("kcg:relatednews", HippoBean.class);
	}

	@HippoEssentialsGenerated(internalName = "kcg:geolocation")
	public Geolocation getGeolocation() {
		return getBean("kcg:geolocation", Geolocation.class);
	}
}
