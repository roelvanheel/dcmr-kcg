jQuery(document).ready(function($){

  /* Slider event handlers
   *
   */   
  var current_slide    = 1;
  var slides_container = $('#slider-list');
  var slides           = $(slides_container).find('li');
  var total_slides     = $(slides).length;
  var slide_width      = $(slides[0]).width();
  
  //Events for the buttons
  function check_slides()
  { 
    //hide or show previous button
    if(current_slide > 1){
      $('#previous').show();
    }else{
      $('#previous').hide();
    }
  
    //clone slides
    if( current_slide % (total_slides - 3) == 0 ){
      $(slides).clone().appendTo('#slider-list');
      total_slides = $(slides_container).find('li').length;
      $(slides_container).width((total_slides + 2) * slide_width);
    }
  }
  
  //Events for the button "previous"
  $('#previous').click(function(){
    current_slide = (current_slide - 1 >= 1 ? current_slide-1 : 1);
    $(slides_container).stop().animate({ "margin-left": -(current_slide * slide_width) + slide_width }, 1000 );
    check_slides();
  });

  //Events for the button "next"
  $('#next').click(function(){
    current_slide = (current_slide + 1 <= total_slides ? current_slide+1 : 1);
    $(slides_container).stop().animate({ "margin-left": -(current_slide * slide_width) + slide_width }, 1000 );
    check_slides();
  });
  
  //Init
  $('#previous').hide();

});
