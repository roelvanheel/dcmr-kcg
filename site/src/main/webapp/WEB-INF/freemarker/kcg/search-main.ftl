<#include "../include/imports.ftl">
<@hst.setBundle basename="kcg.search"/>
<div class="body-wrapper">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <h2><@fmt.message key="kcg.search.title" var="title"/>${title?html} '${query?html}'</h2>
            <#if pageable??>
              <#if pageable.total == 0>
                  <h4><@fmt.message key="kcg.search.noresults" var="noresults"/>${noresults?html} '${query?html}'</h4>
              <#else>
                  <h4><@fmt.message key="kcg.search.displaytitle_start" var="displayTitleStart"/>${displayTitleStart?html} ${pageable.startOffset +1 } <@fmt.message key="kcg.search.displaytitle_to" var="displayTitleTo"/>${displayTitleTo?html} ${pageable.endOffset} <@fmt.message key="kcg.search.displaytitle_of" var="displayTitleOf"/>${displayTitleOf?html} ${pageable.total} <@fmt.message key="kcg.search.displaytitle_resultsfor" var="displayTitleResultsFor"/>${displayTitleResultsFor?html} '${query?html}'</h4>
                  <div id="search-results">
                    <#list pageable.items as item>
                      <@hst.link var="link" hippobean=item />
                        <div class="blog-post">
                            <div class="blog-post-type">
                                <i class="icon-shop"> </i>
                            </div>
                            <div class="blog-span">
                                <b>
                                    <a href="${link}">${item.title?html}</a>
                                </b>
                                <div class="blog-post-body">
                                <#if item.introduction ??>
                                    <p>${item.introduction?html}</p>
                                </#if>
                                </div>
                                <div class="blog-post-details">
                                    <div class="blog-post-details-item blog-post-details-item-right">
                                        <a href="${link}"><@fmt.message key="kcg.search.readmore" var="linkReadmore"/>${linkReadmore?html}<i
                                                class="fa fa-chevron-right"></i>
                                        </a>
                                    </div>
                                </div>
                            <hr/>
                            </div>
                        </div>
                    </#list>
                  </div>
                <#if cparam.showPagination>
                  <#include "../include/pagination.ftl">
                </#if>
              </#if>
            <#else>
        <h3><@fmt.message key="kcg.search.fill_in" var="fillIn"/>${fillIn?html}</h3>
            </#if>

            </div>
        </div>
    </div>