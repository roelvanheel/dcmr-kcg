<#include "/WEB-INF/freemarker/include/imports.ftl">
<@hst.setBundle basename="kcg.products"/>
<#if pageable??>
  <div class="yui-main">
    <div id="content" class="yui-b left-and-right">
      <h2><@fmt.message key="kcg.products.list-title" var="list-title"/>${list-title?html}</h2>
      <div id="products">
        <#list pageable.items as item>
          <@hst.link var="link" hippobean=item/>
          <ul class="product-item">
            <li class="title"><a href="${link}">${item.title}</a></li>
            <li class="price"><a href="${link}"><@fmt.formatNumber value="${item.price}" type="currency" /></a> |</li>
            <#--<li class="rating stars-0"><a href="products-detail.html">0.0</a></li>-->
            <li class="description">${item.introduction}</li>
          </ul>
        </#list>                
      </div>
      <#if pageable.showPagination>
        <#include "/WEB-INF/freemarker/include/pagination.ftl">
      </#if>          
    </div>
  </div>
</#if>