<#include "/WEB-INF/freemarker/include/imports.ftl">

<@hst.setBundle basename="kcg.news"/>
<#if facets??>
<h2 class="left-title"><@fmt.message key="kcg.news.facettitle" var="title"/>${title?html}</h2>
    <ul id="submenu" class="facet">
        <#list facets.folders as facet>
            <li><span>${facet.name}:</span>
                <ul class="bullet-points">
                    <#list facet.folders as value>
                        <#if value.leaf>
                            <@hst.facetnavigationlink var="link" remove=value current=facets />
                            <li><a class="filter" href="${link}">${value.name}</a></li>
                        <#else>
                            <@hst.link var="link" hippobean=value />
                            <li><a href="${link}">${value.name}&nbsp;(${value.count})</a></li>
                        </#if>
                    </#list>
                </ul>
            </li>
        </#list>
    </ul>
</#if>