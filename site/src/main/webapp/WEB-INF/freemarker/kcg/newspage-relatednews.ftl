    <#include "/WEB-INF/freemarker/include/imports.ftl">

    <#if document.relatednews?? && document.relatednews?has_content>
        <h2 class="left-title">Related News</h2>
        <ul id="submenu">

                            <#list document.relatednews as item>
                                <@hst.link var="link" hippobean=item />
                                <li>
                                    <a href="${link}">${item.title?html}</a>
                                </li>
                            </#list>
        </ul>
    </#if>
    