    <#include "/WEB-INF/freemarker/include/imports.ftl">
<#--
  Copyright 2014 Hippo B.V. (http://www.onehippo.com)

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

<@hst.setBundle basename="kcg.footer"/>
<div id="footer" class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<hr></hr>	
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
      <@fmt.message key="kcg.footer.disclaimer" var="disclaimer"/>${disclaimer?html}
	</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">		
		<@hst.include ref="column2"/>
	</div>		
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
		<@hst.include ref="column3"/>
	</div>		
</div>